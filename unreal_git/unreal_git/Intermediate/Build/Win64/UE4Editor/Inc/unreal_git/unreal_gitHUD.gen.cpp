// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "unreal_git/unreal_gitHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeunreal_gitHUD() {}
// Cross Module References
	UNREAL_GIT_API UClass* Z_Construct_UClass_Aunreal_gitHUD_NoRegister();
	UNREAL_GIT_API UClass* Z_Construct_UClass_Aunreal_gitHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_unreal_git();
// End Cross Module References
	void Aunreal_gitHUD::StaticRegisterNativesAunreal_gitHUD()
	{
	}
	UClass* Z_Construct_UClass_Aunreal_gitHUD_NoRegister()
	{
		return Aunreal_gitHUD::StaticClass();
	}
	struct Z_Construct_UClass_Aunreal_gitHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Aunreal_gitHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_git,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Aunreal_gitHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "unreal_gitHUD.h" },
		{ "ModuleRelativePath", "unreal_gitHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Aunreal_gitHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Aunreal_gitHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Aunreal_gitHUD_Statics::ClassParams = {
		&Aunreal_gitHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_Aunreal_gitHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Aunreal_gitHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Aunreal_gitHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Aunreal_gitHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Aunreal_gitHUD, 3094886662);
	template<> UNREAL_GIT_API UClass* StaticClass<Aunreal_gitHUD>()
	{
		return Aunreal_gitHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Aunreal_gitHUD(Z_Construct_UClass_Aunreal_gitHUD, &Aunreal_gitHUD::StaticClass, TEXT("/Script/unreal_git"), TEXT("Aunreal_gitHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Aunreal_gitHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
