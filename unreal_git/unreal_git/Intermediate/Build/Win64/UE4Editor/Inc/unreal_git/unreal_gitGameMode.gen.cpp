// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "unreal_git/unreal_gitGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeunreal_gitGameMode() {}
// Cross Module References
	UNREAL_GIT_API UClass* Z_Construct_UClass_Aunreal_gitGameMode_NoRegister();
	UNREAL_GIT_API UClass* Z_Construct_UClass_Aunreal_gitGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_unreal_git();
// End Cross Module References
	void Aunreal_gitGameMode::StaticRegisterNativesAunreal_gitGameMode()
	{
	}
	UClass* Z_Construct_UClass_Aunreal_gitGameMode_NoRegister()
	{
		return Aunreal_gitGameMode::StaticClass();
	}
	struct Z_Construct_UClass_Aunreal_gitGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Aunreal_gitGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_git,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Aunreal_gitGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "unreal_gitGameMode.h" },
		{ "ModuleRelativePath", "unreal_gitGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Aunreal_gitGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Aunreal_gitGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Aunreal_gitGameMode_Statics::ClassParams = {
		&Aunreal_gitGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_Aunreal_gitGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Aunreal_gitGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Aunreal_gitGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Aunreal_gitGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Aunreal_gitGameMode, 2611194393);
	template<> UNREAL_GIT_API UClass* StaticClass<Aunreal_gitGameMode>()
	{
		return Aunreal_gitGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Aunreal_gitGameMode(Z_Construct_UClass_Aunreal_gitGameMode, &Aunreal_gitGameMode::StaticClass, TEXT("/Script/unreal_git"), TEXT("Aunreal_gitGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Aunreal_gitGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
