// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREAL_GIT_unreal_gitHUD_generated_h
#error "unreal_gitHUD.generated.h already included, missing '#pragma once' in unreal_gitHUD.h"
#endif
#define UNREAL_GIT_unreal_gitHUD_generated_h

#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_SPARSE_DATA
#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_RPC_WRAPPERS
#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAunreal_gitHUD(); \
	friend struct Z_Construct_UClass_Aunreal_gitHUD_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_gitHUD)


#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAunreal_gitHUD(); \
	friend struct Z_Construct_UClass_Aunreal_gitHUD_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_gitHUD)


#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aunreal_gitHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_gitHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_gitHUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_gitHUD(Aunreal_gitHUD&&); \
	NO_API Aunreal_gitHUD(const Aunreal_gitHUD&); \
public:


#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_gitHUD(Aunreal_gitHUD&&); \
	NO_API Aunreal_gitHUD(const Aunreal_gitHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_gitHUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aunreal_gitHUD)


#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define unreal_git_Source_unreal_git_unreal_gitHUD_h_9_PROLOG
#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_RPC_WRAPPERS \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_INCLASS \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_git_Source_unreal_git_unreal_gitHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_INCLASS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_GIT_API UClass* StaticClass<class Aunreal_gitHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_git_Source_unreal_git_unreal_gitHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
