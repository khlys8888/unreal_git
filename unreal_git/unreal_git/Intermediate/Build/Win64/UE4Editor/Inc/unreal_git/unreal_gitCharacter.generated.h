// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREAL_GIT_unreal_gitCharacter_generated_h
#error "unreal_gitCharacter.generated.h already included, missing '#pragma once' in unreal_gitCharacter.h"
#endif
#define UNREAL_GIT_unreal_gitCharacter_generated_h

#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_SPARSE_DATA
#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_RPC_WRAPPERS
#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAunreal_gitCharacter(); \
	friend struct Z_Construct_UClass_Aunreal_gitCharacter_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_gitCharacter)


#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAunreal_gitCharacter(); \
	friend struct Z_Construct_UClass_Aunreal_gitCharacter_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_gitCharacter)


#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aunreal_gitCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_gitCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_gitCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_gitCharacter(Aunreal_gitCharacter&&); \
	NO_API Aunreal_gitCharacter(const Aunreal_gitCharacter&); \
public:


#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_gitCharacter(Aunreal_gitCharacter&&); \
	NO_API Aunreal_gitCharacter(const Aunreal_gitCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_gitCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aunreal_gitCharacter)


#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(Aunreal_gitCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(Aunreal_gitCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(Aunreal_gitCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(Aunreal_gitCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(Aunreal_gitCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(Aunreal_gitCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(Aunreal_gitCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(Aunreal_gitCharacter, L_MotionController); }


#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_17_PROLOG
#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_RPC_WRAPPERS \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_INCLASS \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_INCLASS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_GIT_API UClass* StaticClass<class Aunreal_gitCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_git_Source_unreal_git_unreal_gitCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
