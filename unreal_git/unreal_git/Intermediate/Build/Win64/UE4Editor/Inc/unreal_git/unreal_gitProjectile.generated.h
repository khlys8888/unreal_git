// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef UNREAL_GIT_unreal_gitProjectile_generated_h
#error "unreal_gitProjectile.generated.h already included, missing '#pragma once' in unreal_gitProjectile.h"
#endif
#define UNREAL_GIT_unreal_gitProjectile_generated_h

#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_SPARSE_DATA
#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAunreal_gitProjectile(); \
	friend struct Z_Construct_UClass_Aunreal_gitProjectile_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_gitProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAunreal_gitProjectile(); \
	friend struct Z_Construct_UClass_Aunreal_gitProjectile_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_gitProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aunreal_gitProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_gitProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_gitProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_gitProjectile(Aunreal_gitProjectile&&); \
	NO_API Aunreal_gitProjectile(const Aunreal_gitProjectile&); \
public:


#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_gitProjectile(Aunreal_gitProjectile&&); \
	NO_API Aunreal_gitProjectile(const Aunreal_gitProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_gitProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aunreal_gitProjectile)


#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(Aunreal_gitProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(Aunreal_gitProjectile, ProjectileMovement); }


#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_12_PROLOG
#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_RPC_WRAPPERS \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_INCLASS \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_INCLASS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_GIT_API UClass* StaticClass<class Aunreal_gitProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_git_Source_unreal_git_unreal_gitProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
