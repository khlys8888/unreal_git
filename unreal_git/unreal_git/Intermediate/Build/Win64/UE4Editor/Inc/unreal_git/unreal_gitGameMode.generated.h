// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREAL_GIT_unreal_gitGameMode_generated_h
#error "unreal_gitGameMode.generated.h already included, missing '#pragma once' in unreal_gitGameMode.h"
#endif
#define UNREAL_GIT_unreal_gitGameMode_generated_h

#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_SPARSE_DATA
#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_RPC_WRAPPERS
#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAunreal_gitGameMode(); \
	friend struct Z_Construct_UClass_Aunreal_gitGameMode_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), UNREAL_GIT_API) \
	DECLARE_SERIALIZER(Aunreal_gitGameMode)


#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAunreal_gitGameMode(); \
	friend struct Z_Construct_UClass_Aunreal_gitGameMode_Statics; \
public: \
	DECLARE_CLASS(Aunreal_gitGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git"), UNREAL_GIT_API) \
	DECLARE_SERIALIZER(Aunreal_gitGameMode)


#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	UNREAL_GIT_API Aunreal_gitGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_gitGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UNREAL_GIT_API, Aunreal_gitGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UNREAL_GIT_API Aunreal_gitGameMode(Aunreal_gitGameMode&&); \
	UNREAL_GIT_API Aunreal_gitGameMode(const Aunreal_gitGameMode&); \
public:


#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UNREAL_GIT_API Aunreal_gitGameMode(Aunreal_gitGameMode&&); \
	UNREAL_GIT_API Aunreal_gitGameMode(const Aunreal_gitGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UNREAL_GIT_API, Aunreal_gitGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_gitGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aunreal_gitGameMode)


#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_9_PROLOG
#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_RPC_WRAPPERS \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_INCLASS \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_SPARSE_DATA \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_INCLASS_NO_PURE_DECLS \
	unreal_git_Source_unreal_git_unreal_gitGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_GIT_API UClass* StaticClass<class Aunreal_gitGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_git_Source_unreal_git_unreal_gitGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
