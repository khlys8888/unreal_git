// Copyright Epic Games, Inc. All Rights Reserved.

#include "unreal_git1GameMode.h"
#include "unreal_git1HUD.h"
#include "unreal_git1Character.h"
#include "UObject/ConstructorHelpers.h"

Aunreal_git1GameMode::Aunreal_git1GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aunreal_git1HUD::StaticClass();
}
