// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "unreal_git1GameMode.generated.h"

UCLASS(minimalapi)
class Aunreal_git1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aunreal_git1GameMode();
};



