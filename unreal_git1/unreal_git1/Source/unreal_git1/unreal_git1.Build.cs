// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class unreal_git1 : ModuleRules
{
	public unreal_git1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
