// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef UNREAL_GIT1_unreal_git1Projectile_generated_h
#error "unreal_git1Projectile.generated.h already included, missing '#pragma once' in unreal_git1Projectile.h"
#endif
#define UNREAL_GIT1_unreal_git1Projectile_generated_h

#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_SPARSE_DATA
#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAunreal_git1Projectile(); \
	friend struct Z_Construct_UClass_Aunreal_git1Projectile_Statics; \
public: \
	DECLARE_CLASS(Aunreal_git1Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git1"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_git1Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAunreal_git1Projectile(); \
	friend struct Z_Construct_UClass_Aunreal_git1Projectile_Statics; \
public: \
	DECLARE_CLASS(Aunreal_git1Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git1"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_git1Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aunreal_git1Projectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_git1Projectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_git1Projectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_git1Projectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_git1Projectile(Aunreal_git1Projectile&&); \
	NO_API Aunreal_git1Projectile(const Aunreal_git1Projectile&); \
public:


#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_git1Projectile(Aunreal_git1Projectile&&); \
	NO_API Aunreal_git1Projectile(const Aunreal_git1Projectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_git1Projectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_git1Projectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aunreal_git1Projectile)


#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(Aunreal_git1Projectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(Aunreal_git1Projectile, ProjectileMovement); }


#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_12_PROLOG
#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_SPARSE_DATA \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_RPC_WRAPPERS \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_INCLASS \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_SPARSE_DATA \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_INCLASS_NO_PURE_DECLS \
	unreal_git1_Source_unreal_git1_unreal_git1Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_GIT1_API UClass* StaticClass<class Aunreal_git1Projectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_git1_Source_unreal_git1_unreal_git1Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
