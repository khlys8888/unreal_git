// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "unreal_git1/unreal_git1HUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeunreal_git1HUD() {}
// Cross Module References
	UNREAL_GIT1_API UClass* Z_Construct_UClass_Aunreal_git1HUD_NoRegister();
	UNREAL_GIT1_API UClass* Z_Construct_UClass_Aunreal_git1HUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_unreal_git1();
// End Cross Module References
	void Aunreal_git1HUD::StaticRegisterNativesAunreal_git1HUD()
	{
	}
	UClass* Z_Construct_UClass_Aunreal_git1HUD_NoRegister()
	{
		return Aunreal_git1HUD::StaticClass();
	}
	struct Z_Construct_UClass_Aunreal_git1HUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Aunreal_git1HUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_git1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Aunreal_git1HUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "unreal_git1HUD.h" },
		{ "ModuleRelativePath", "unreal_git1HUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Aunreal_git1HUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Aunreal_git1HUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Aunreal_git1HUD_Statics::ClassParams = {
		&Aunreal_git1HUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_Aunreal_git1HUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Aunreal_git1HUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Aunreal_git1HUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Aunreal_git1HUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Aunreal_git1HUD, 907294106);
	template<> UNREAL_GIT1_API UClass* StaticClass<Aunreal_git1HUD>()
	{
		return Aunreal_git1HUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Aunreal_git1HUD(Z_Construct_UClass_Aunreal_git1HUD, &Aunreal_git1HUD::StaticClass, TEXT("/Script/unreal_git1"), TEXT("Aunreal_git1HUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Aunreal_git1HUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
