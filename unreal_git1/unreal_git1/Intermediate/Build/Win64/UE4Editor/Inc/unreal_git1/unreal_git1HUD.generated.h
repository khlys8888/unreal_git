// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREAL_GIT1_unreal_git1HUD_generated_h
#error "unreal_git1HUD.generated.h already included, missing '#pragma once' in unreal_git1HUD.h"
#endif
#define UNREAL_GIT1_unreal_git1HUD_generated_h

#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_SPARSE_DATA
#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_RPC_WRAPPERS
#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAunreal_git1HUD(); \
	friend struct Z_Construct_UClass_Aunreal_git1HUD_Statics; \
public: \
	DECLARE_CLASS(Aunreal_git1HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git1"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_git1HUD)


#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAunreal_git1HUD(); \
	friend struct Z_Construct_UClass_Aunreal_git1HUD_Statics; \
public: \
	DECLARE_CLASS(Aunreal_git1HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_git1"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_git1HUD)


#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aunreal_git1HUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_git1HUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_git1HUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_git1HUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_git1HUD(Aunreal_git1HUD&&); \
	NO_API Aunreal_git1HUD(const Aunreal_git1HUD&); \
public:


#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_git1HUD(Aunreal_git1HUD&&); \
	NO_API Aunreal_git1HUD(const Aunreal_git1HUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_git1HUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_git1HUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aunreal_git1HUD)


#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_PRIVATE_PROPERTY_OFFSET
#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_9_PROLOG
#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_SPARSE_DATA \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_RPC_WRAPPERS \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_INCLASS \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_SPARSE_DATA \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_INCLASS_NO_PURE_DECLS \
	unreal_git1_Source_unreal_git1_unreal_git1HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_GIT1_API UClass* StaticClass<class Aunreal_git1HUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_git1_Source_unreal_git1_unreal_git1HUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
